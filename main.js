var _ = require('lodash');

var spawn1 = Game.spawns.Spawn1;
var spawnOrder = ["Harvester", "Healer", "Guard", "Harvester", "Guard", "Harvester", "Healer"];
//var spawnOrder = ["Harvester", "Harvester", "Turret", "Harvester", "Turret", "Guard", "Healer", "Guard", "Healer"];
var main = function() {
	if (!spawn1.spawning) {
		if (!spawn1.memory.spawnIndex) { //initialize spawn index
			spawn1.memory.spawnIndex = 0;
		}
		var creepType = spawnOrder[spawn1.memory.spawnIndex];
		var max;
		if (creepType == "Harvester") {
			max = 3;
		} else if (creepType == "Guard") {
			max = 20;
		} else if (creepType == "Healer") {
			max = 2;
		} else if (creepType == "Turret") {
			max = 2;
		}
		spawnCreep(creepType, max);
		spawn1.memory.spawnIndex = ++spawn1.memory.spawnIndex % spawnOrder.length;
	}
	for (var creepName in Game.creeps) {
		var creep = Game.creeps[creepName];
		creepTypes[creep.memory.role].behavior(creep);
	}
};

var spawnCreep = function(creepType, max) {
	var creeps = spawn1.room.find(Game.MY_CREEPS, {
		filter: function(creep) {
			return creep.memory.role == creepType;
		}
	});
	var body = creepTypes[creepType].body;
	var energyReq = energyRequirement(body);
	if (_.size(creeps) < max && spawn1.energy >= energyReq) {
		console.log("Spawning " + creepType + " with cost " + energyReq);
		if (!spawn1.memory[creepType + "Count"]) {
			spawn1.memory[creepType + "Count"] = 1;
		} else {
			spawn1.memory[creepType + "Count"]++;
		}
		var creepName = creepType + "." + spawn1.memory[creepType + "Count"];
		spawn1.createCreep(body, creepName, {role: creepType});
		return true;
	} else {
	    return false;
	}
};

var creepTypes = {
	"Harvester": {
		body: [Game.WORK, Game.CARRY, Game.MOVE],
		behavior: function (creep) {
			if(creep.energy < creep.energyCapacity) {
				var sources = creep.room.find(Game.SOURCES);
				creep.moveTo(sources[0]);
				creep.harvest(sources[0]);
			}
			else {
				creep.moveTo(Game.spawns.Spawn1);
				creep.transferEnergy(Game.spawns.Spawn1);
			}
		}
	},
	"Guard": {
		//USE WITH MOBILE HEALER
		/*
		body: [Game.TOUGH, Game.ATTACK, Game.MOVE, Game.MOVE],
		behavior: function(creep) {
			var target = creep.pos.findNearest(Game.HOSTILE_CREEPS, { filter: hostileFilter });
			if (target && readyToFight(creep) && spawn1.pos.inRangeTo(target.pos, 10)) {
				creep.moveTo(target);
				creep.attack(target);
			} else {
				creep.moveTo(spawn1);
			}
		}
		*/
		//USE WITH STATIONARY HEALER
		body: [Game.TOUGH, Game.ATTACK, Game.MOVE, Game.MOVE],
		behavior: function(creep) {
			var target = creep.pos.findNearest(Game.HOSTILE_CREEPS, { filter: hostileFilter });
			if (target && readyToFight(creep)) { //no range check, so fight far away from spawn
				creep.moveTo(target);
				creep.attack(target);
			} else {
				var healer = creep.pos.findNearest(Game.MY_CREEPS, {
					filter: function(myCreep) {
						return myCreep.memory.role == "Healer";
					} 
				});
				creep.moveTo(healer); //retreat to healer
			}
		}
	},
	"Builder": {
		body: [Game.WORK, Game.WORK, Game.WORK, Game.CARRY, Game.MOVE],
		behavior: function(creep) {
			if(creep.energy === 0) {
				creep.moveTo(spawn1);
				Game.spawns.Spawn1.transferEnergy(creep);
			}
			else {
				var targets = creep.room.find(Game.CONSTRUCTION_SITES);
				if(targets.length) {
					creep.moveTo(targets[0]);
					creep.build(targets[0]);
				}
			}
		}
	},
	"Healer": {
		//MOBILE HEALER
		/*
		body: [Game.MOVE, Game.HEAL],
		behavior: function(creep) {
			var targets = creep.room.find(Game.MY_CREEPS, {
				filter: function(myCreep) {
					return myCreep.memory.role == "Guard";
				}
			});
			if (targets.length && readyToHeal(creep)) {
				var mostInjured = getMostInjured(targets);
				creep.moveTo(mostInjured);
				creep.heal(mostInjured);
			} else {
				creep.moveTo(spawn1);
			}
		}
		*/
		//STATIONARY HEALER (at rally)
		body: [Game.MOVE, Game.HEAL],
		behavior: function(creep) {
			var rally = creep.room.find(Game.FLAGS);
			if (rally.length) {
				creep.moveTo(rally[0]);
			}
			var targets = creep.pos.findInRange(Game.MY_CREEPS, 1, {
				filter: function(myCreep) {
					return myCreep.memory.role == "Guard";
				}
			});
			if (targets.length) {
				var mostInjured = getMostInjured(targets);
				creep.heal(mostInjured);
			}
		}
	},
	"Turret": {
		body: [Game.MOVE, Game.RANGED_ATTACK],
		behavior: function(creep) {
			var targets = creep.pos.findInRange(Game.HOSTILE_CREEPS, 3, { filter: hostileFilter });
			if (targets.length) {
				creep.rangedAttack(targets[0]);
			}
		}
	}
};


//ignore hostiles with 0 move and 0 range attack
var hostileFilter = function(hostile) {
	var attack = false;
	_(hostile.body).forEach(function(bodyPart) {
		if ((bodyPart.type == "move" || bodyPart.type == "ranged_attack") && bodyPart.hits > 0) {
			attack = true;
		}
	});
	return attack;
};

var getMostInjured = function(creeps) {
	var maxInjury = 0;
	var maxInjured = creeps[0];
	for (var creepIndex in creeps) {
		var creep = creeps[creepIndex];
		var injury = 0;
		for (var bodyPartIndex in creep.body) {
			var bodyPart = creep.body[bodyPartIndex];
			injury += 100 - bodyPart.hits;
		}
		if (injury > maxInjury) {
			maxInjury = injury;
			maxInjured = creep;
		}
	}
	return maxInjured;
};

//decide whether this Guard should fight or retreat
var readyToFight = function(creep) {
	var ready = true;
	_(creep.body).forEach(function(bodyPart) {
		if (bodyPart.type == "attack" && bodyPart.hits === 0) {
			ready = false;
			return false;
		}
	});
	if (!ready) {
		console.log("Guard retreat!");
	}
	return ready;
};

//tell whether this Healer should heal or retreat
var readyToHeal = function(creep) {
	var ready = true;
	_(creep.body).forEach(function(bodyPart) {
		if (bodyPart.type == "heal" && bodyPart.hits < 100) {
			ready = false;
			return false;
		}
	});
	if (!ready) {
		console.log("Healer retreat!");
	}
	return ready;
};

var energyRequirement = function(creepBody) {
	var energy = 0;
	for (var partIndex in creepBody) {
		var bodyPart = creepBody[partIndex];
		switch (bodyPart) {
			case Game.MOVE:
				energy+=50;
				break;
			case Game.WORK:
				energy+=20;
				break;
			case Game.CARRY:
				energy+=50;
				break;
			case Game.ATTACK:
				energy+=100;
				break;
			case Game.RANGED_ATTACK:
				energy+=150;
				break;
			case Game.HEAL:
				energy+=200;
				break;
			case Game.TOUGH:
				energy+=5;
				break;
			default:
				break;
		}
	}
	return energy;
};

main();